# JavaScript基础包

#### 项目介绍
JavaScript常用的方法大集合，
里面有对json处理的各种方法，
对时间处理的各种方法。
从手机端，PC端，从ie5到ie11都兼容。
整个项目都是免费开源的。
可以用于开发大型项目。
附件里面有整个文档，还有实验，测试，环境工具。

#### 软件架构
软件架构说明


#### 安装教程

1. 下载附件，附件里面有2个js文件，一个是utilFn.js，另一个是压缩版utilFn.min.js。
2. 引用<script src="../utilFn.js"></script>就可以使用里面的方法。
3. 或者引用<script src="../utilFn.min.js"></script>
4.utilFn.js的所有方法都是独立的，你需要的方法可以直接复制使用。

#### 使用说明

1. 使用很简单，用浏览器打开附件里面的index.html网页。
2. 里面整体方法都，整理非常完善。看一下就会了。
3. 附件里面有相关说明，附件里面也有实验和测试环境。

#### 参与贡献

1. 数据分析系统开发
2. 大数据前端开发。
3. 营销系统开发
4. 问卷全兼容模式系统开发。（包括兼容IE5到ie 11,兼容google，兼容手机浏览器微信的内置浏览器，uc浏览器等）


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)